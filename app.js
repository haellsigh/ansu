var _ = require('underscore');
var low = require('lowdb');
var db = low('data\animes.json');
var program = require('commander');

program
	.version('0.1.0')
	.option('-a, --add [animes]', 'Add anime(s) [animes] to the database separated with \';\'')
	.parse(process.argv);
	
console.log('Adding anime(s): ');
console.log(program.add);

program.add.split(';')